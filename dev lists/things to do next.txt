Coding related:
-bag integration
-move integration
-interaction handler


Non-coding related:
-Storyboard creation
-Overall map layout design
-Detailed town design
-Detailed pathway design
-List of pokes with parameters: moves with proper relation to type and/or character, level move is obtained, evolutions and evolution levels, properties (high hp, ect.), names, sprites
-List of moves with parameters: properties of the move according to moves.ini, proper naming relations and conventions in accordance with pokes, then rebalanced
-Building sprites
-Interior sprites
-Overworld connected in relation to storyboard and overall map layout design