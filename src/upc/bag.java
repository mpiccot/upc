/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public class bag {
    private ArrayList<bagitem> baglist = new ArrayList<>();
    public bag(){}
    public void addPoke(poke poke, int level){
        bagitem tempitem = new bagitem();
        tempitem.updateID(poke.getID());
        tempitem.setHP(poke.getHP() * level);
        tempitem.setStamina(poke.getStamina() * level);
        //IDEA: add a move, 2 arguments, the move ID, and the move to be replaced (move 0-3)
        //need a way to get the highest 4 move IDs based on level
        tempitem.updateMove(poke.getMove(0), 0);
        tempitem.updateMove(poke.getMove(1), 1);
        tempitem.updateMove(poke.getMove(2), 2);
        tempitem.updateMove(poke.getMove(3), 3);
        tempitem.updateLevel(level);   
        baglist.add(tempitem);  
    }
    public void dropPoke(int bagIndex){
        baglist.remove(bagIndex);
    }
    public bagitem getItem(int bagIndex){
        return baglist.get(bagIndex);
    }
}
