/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;


/**
 *
 * @author Mike
 */
public class gridhandler {
    private int movedistance, mapwidth, mapheight;
    String filename;
    Boolean[][] grid;
    public gridhandler(int movedistance, int mapwidth, int mapheight, String datafile){
        this.movedistance = movedistance;
        this.mapwidth = mapwidth;
        this.mapheight = mapheight;
        filename = datafile;
    }
    public void makeGrid() throws FileNotFoundException, IOException{
        System.out.println(mapwidth/movedistance);
        grid = new Boolean[(mapwidth/movedistance)][(mapheight/movedistance)];
        for (Boolean[] row: grid)
            Arrays.fill(row, true);
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line, number = "";
        int i, j = 0, k, l;
        //x, y, width, height
        int[] params = new int[4];
        boolean para = false;
        while ((line = br.readLine()) != null){
            for (i = 0; i < line.length(); i++){
                if (line.charAt(i) == '('){
                    para = true;
                    j = 0;
                    number = "";
                }
                else if (line.charAt(i) == ')'){
                    params[j] = Integer.parseInt(number);
                    j++;
                    para = false;
                }
                else if (line.charAt(i) == ','){
                    params[j] = Integer.parseInt(number);
                    number = "";
                    j++;
                }
                else if (line.charAt(i) == ';'){
                    para = false;
                    j = 0;
                    number = "";
                    i = line.length();
                }
                else
                    number += line.charAt(i);
                //if you get to j = 4, you are done with a set of params, so export
                if (j == 4){
                    for (k = 0; k < params[2]; k++){
                        for (l = 0; l < params[3]; l++){
                            grid[(params[0]/movedistance)+(k/movedistance)][(params[1]/movedistance)+(l/movedistance)] = false;
                        }
                    }
                }
            }
        }
        br.close();
    }
    public String outGrid(){
        String str = "";
        for (int i = 0; i < (mapwidth/movedistance); i++){
            for (int j = 0; j < (mapheight/movedistance); j++){
                str += grid[i][j].toString();
            }
        }
        return str;
    }
    public void changeValue(int x, int y, boolean value){
        grid[x/movedistance][y/movedistance] = value;
    }
    //x and y are grid terms, not coords
    public boolean getVal(int x,int y){
        return grid[x][y];
    }
    //x and y are coords
    public boolean isLegal(int x, int y){
        return grid[(x/movedistance)][(y/movedistance)];
    }
            
}
