/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public class wormholehandler {
    private ArrayList<wormhole> worm = new ArrayList<>();
    private gridhandler grid;
    //im pretty sure this passes by refrence, not sure if by constant
    //if you make any changes to grid aften the initial setup, this could cause issues here
    public wormholehandler(gridhandler grid){
        this.grid = grid;
    }
    public void addWormhole(int inx, int iny, int outx, int outy, int screenWidth, int screenHeight){
   //     wormhole temphole = new wormhole(inx);
   //     worm.add(temphole);
        worm.add(new wormhole(inx, iny, outx, outy, screenWidth, screenHeight, grid));
    }
    public void wormholeReader(int screenWidth, int screenHeight, String data) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(data));
        String line, number = "";
        int i, j = 0, k, l;
        //inx, iny, outx, outy
        int[] params = new int[4];
        boolean para = false;
        while ((line = br.readLine()) != null){
            for (i = 0; i < line.length(); i++){
                if (line.charAt(i) == '('){
                    para = true;
                    j = 0;
                    number = "";
                }
                else if (line.charAt(i) == ')'){
                    params[j] = Integer.parseInt(number);
                    j++;
                    para = false;
                }
                else if (line.charAt(i) == ','){
                    params[j] = Integer.parseInt(number);
                    number = "";
                    j++;
                }
                else if (line.charAt(i) == ';'){
                    para = false;
                    j = 0;
                    number = "";
                    i = line.length();
                }
                else
                    number += line.charAt(i);
                //if you get to j = 4, you are done with a set of params, so export
                if (j == 4){
                    addWormhole(params[0], params[1], params[2], params[3], screenWidth, screenHeight);
                }
            }
        }
        br.close();
    }
    public wormhole checkForWormhole(int inx, int iny){
        for (int i = 0; i < worm.size(); i++){
            if (inx == worm.get(i).getInX()){
                if (iny == worm.get(i).getInY())
                    return worm.get(i);
            }
        }
        return null;
    }
    //DEBUGGING ONLY
    public int getFirstCoords(){
        return worm.get(0).getInX();
    }
}
