/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

/**
 *
 * @author Mike
 */
public class wormhole {
    private int inx, iny, outx, outy;
    //im pretty sure this passes by refrence, not sure if by constant
    //if you make any changes to grid aften the initial setup, this could cause issues here
    public wormhole(int inx, int iny, int outx, int outy, int screenWidth, int screenHeight, gridhandler grid){
        this.inx = inx - (screenWidth/2);
        this.iny = iny - (screenHeight/2);
        this.outx = outx - (screenWidth/2);
        this.outy = outy - (screenHeight/2);
        grid.changeValue(this.inx+300, this.iny+300, true);
        grid.changeValue(this.outx+300, this.outy+300, true);
    }
    public int getInX(){
        return inx;
    }
    public int getInY(){
        return iny;
    }
    public int getOutX(){
        return outx;
    }
    public int getOutY(){
        return outy;
    }
}
