/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public class pokehandler {
    private ArrayList<poke> poke = new ArrayList<>();
    public pokehandler(){}
    public void pokeReader(String data) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(data));
        String line = "", tempstring = "";
        int linenum = 0;
        int[] params = new int[5];
        poke temppoke = new poke();
        ArrayList<Integer> temparray = new ArrayList<>();
        while ((line = br.readLine()) != null){
            if (line.length() > 0){
            if (line.charAt(0) != ';'){
                if (linenum == 0){
                    temppoke = new poke();
                    temppoke.updateName(line.substring(5));
                    linenum++;
                }
                else if (linenum < 6){
                    params[linenum-1] = Integer.parseInt(line.substring(7));
                    linenum++;
                }
                else if (linenum == 6){
                    temparray.clear();
                    temppoke.updateID(params[0]);
                    temppoke.updateHP(params[1]);
                    temppoke.updateStamina(params[2]);
                    temppoke.updateEvolvesTo(params[3]);
                    temppoke.updateEvolveLevel(params[4]);
                    line = line.substring(6);
                    while (line.isEmpty() == false){
                        temparray.add(Integer.parseInt(line.substring(1, 4)));
                        line = line.substring(4);
                    }
                    temppoke.updateMoveID(temparray);
                    linenum++;
                }
                else if (linenum == 7){
                    temparray.clear();
                    line = line.substring(6);
                    while (line.isEmpty() == false){
                        temparray.add(Integer.parseInt(line.substring(1, 4)));
                        line = line.substring(4);
                    }
                    temppoke.updateMoveLevel(temparray);
                    linenum++;
                }
            }
            }
            if (linenum == 8){
                
                    addPoke(temppoke);
                    //clear params
                    linenum = 0;
                    tempstring = "";
                    temparray.clear();
                }
        }
        br.close();
    }
    public void addPoke(poke little){
        poke.add(little);
    }
    public poke getPoke(int ID){
        return poke.get(ID);
    }
    //DEBUGGING
    public String returnPoke(int i){
        return (poke.get(i).getPokeInfo() + "[]" + poke.get(i).getPokeMoves());
    }
}
