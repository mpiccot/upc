/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

import java.awt.List;
import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public class poke {
    private String name;
    private int hp, stamina, evolvesTo, id, evolveLevel;
    private ArrayList<Integer> moveID = new ArrayList<>();
    private ArrayList<Integer> moveLevel = new ArrayList<>();
    public poke(){}
    public void updateName(String name){
        this.name = name;
    }
    public void updateID(int id){
        this.id = id;
    }
    public void updateHP(int hp){
        this.hp = hp;
    }
    public void updateStamina(int stamina){
        this.stamina = stamina;
    }
    public void updateEvolvesTo(int evolvesTo){
        this.evolvesTo = evolvesTo;
    }
    public void updateEvolveLevel(int evolveLevel){
        this.evolveLevel = evolveLevel;
    }
    public void updateMoveID(ArrayList<Integer> moveID){
        for (int i = 0; i < moveID.size(); i++){
            this.moveID.add(moveID.get(i));
        }
    }
    public void updateMoveLevel(ArrayList<Integer> moveLevel){
        for (int i = 0; i < moveLevel.size(); i++){
            this.moveLevel.add(moveLevel.get(i));
        }
    }
    //DEBUGGING
    public String getPokeInfo(){
        return (name + "," + Integer.toString(id) + "," + Integer.toString(hp) + "," + Integer.toString(stamina) + "," + Integer.toString(evolvesTo) + "," + Integer.toString(evolveLevel));
    }
    //DEBUGGING
    public String getPokeMoves(){
        String str1 = "", str2 = "";
        for (int i = 0; i < moveID.size(); i++){
            str1 += Integer.toString(moveID.get(i)) + ",";
        }
        for (int i = 0; i < moveLevel.size(); i++){
            str2 += Integer.toString(moveLevel.get(i)) + ",";
        }
        return (str1 + "[]" + str2);
    }
    public int getMove(int index){
        return moveID.get(index);
    }
    public String getName(){
        return name;
    }
    public int getID(){
        return id;
    }
    public int getHP(){
        return hp;
    }
    public int getStamina(){
        return stamina;
    }
    public int getEvolvesTo(){
        return evolvesTo;
    }
    public int getEvolveLevel(){
        return evolveLevel;
    }
}
