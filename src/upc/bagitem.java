/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

/**
 *
 * @author Mike
 */
//Contains 4 moves, currentstam, currenthp, 
public class bagitem {
    private int currentStamina, currentHP, level, ID;
    private int[] moves = new int[4];
    public bagitem(){}
    public void setStamina(int currentStamina){
        this.currentStamina = currentStamina;
    }
    public void setHP(int currentHP){
        this.currentHP = currentHP;
    }
    public void subStamina(int changeStamina){
        this.currentStamina = this.currentStamina - changeStamina;
    }
    public void subHP(int changeHP){
        this.currentHP = this.currentHP - changeHP;
    }
    public void updateLevel(int level){
        this.level = level;
    }
    public void updateID(int ID){
        this.ID = ID;
    }
    public void updateMoves(int[] moves){
        this.moves[0] = moves[0];
        this.moves[1] = moves[1];
        this.moves[2] = moves[2];
        this.moves[3] = moves[3];
    }
    //Clear slot 1-3
    public void updateMove(int newMove, int clearSlot){
        moves[clearSlot] = newMove;
    }
    public int getStamina(){
        return currentStamina;
    }
    public int getHP(){
        return currentHP;
    }
    public int getLevel(){
        return level;
    }
    public int[] getMoves(){
        return moves;
    }
    public int getID(){
        return ID;
    }
    public int getMove1(){
        return moves[0];
    }
    public int getMove2(){
        return moves[1];
    }
    public int getMove3(){
        return moves[2];
    }
    public int getMove4(){
        return moves[3];
    }
}
