package upc;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mike
 */
public class move {
    private String name, element;
    //All attacks do damage if there is a damage
    //Negative damage hurts yourself, IGNORES DURATION, THIS ONLY HITS YOU FOR THE LISTED DAMAGE
    //All attacks apply a debuff if there is a modstat1 and/or modstat2 for debuffduration and is used on enemy if modenemy = true
    //All attacks heal you if there is an absorb
    //Damage attacks are DOTs if there is a duration, hitting for damage each turn for duration, heals also apply over duration
    //The type flag modifies these rules in the following way:
    //1: normal rules apply
    //2: 2 turn damage attack (heals and mods applied instantly, damage on second turn)
    //4: invul for 1 turn (heals and mods applied instantly)
    //8: absorbs taken damage per turn equal to absorb for duration REPLACES the heal MUST have duration (note if there was damage, its changed to a dot)
    //16: 2x damage if last enemy move was attack, otherwise normal rules
    //32: recovers stamina equal to absorb over duration if there is a duration REPLACES THE HEAL (note, damage is auto changed to a dot)
    private int type, damage, absorb, modstat1, modstat2, modamount1, modamount2, duration, debuffDuration, id;
    private boolean forceFirst, forceLast, modEnemy1, modEnemy2;
    private ArrayList<Integer> pokeID = new ArrayList<>();
    private ArrayList<String> newName = new ArrayList<>();
    public move(){}
    //If blank, ignored
    public void updatePokeIDList(ArrayList<Integer> pokeID){
        for (int i = 0; i < pokeID.size(); i++){
            this.pokeID.add(pokeID.get(i));
        }
    }
    //If blank ignored
    public void updateNameList(ArrayList<String> newName){
        for (int i = 0; i < newName.size(); i++){
            this.newName.add(newName.get(i));
        }
    }
    //Required
    public void updateName(String name){
        this.name = name;
    }
    //Required
    public void updateElement(String element){
        this.element = element;
    }
    //Required
    public void updateID(int id){
        this.id = id;
    }
    //Required
    public void updateType(int type){
        this.type = type;
    }
    //Ignored if 0
    public void updateDamage(int damage){
        this.damage = damage;
    }
    //Ignored if 0
    public void updateAbsorb(int absorb){
        this.absorb = absorb;
    }
    //Ignored if 0
    public void updateModstat1(int modstat1){
        this.modstat1 = modstat1;
    }
    //Ignored if 0
    public void updateModstat2(int modstat2){
        this.modstat2 = modstat2;
    }
    //Ignored if modstat1 = 0
    public void updateModamount1(int modamount1){
        this.modamount1 = modamount1;
    }
    //Ignored if modstat2 = 0
    public void updateModamount2(int modamount2){
        this.modamount2 = modamount2;
    }
    //Required (0 indicates normal attacks, ect)
    public void updateDuration(int duration){
        this.duration = duration;
    }
    //Ignored if modstat1 and modstat2 = 0
    public void updateDebuffDuration(int debuffDuration){
        this.debuffDuration = debuffDuration;
    }
    //Required
    public void updateForceFirst(boolean forceFirst){
        this.forceFirst = forceFirst;
    }
    //Required
    public void updateForceLast(boolean forceLast){
        this.forceLast = forceLast;
    }
    //Ignored if modstat1 = 0
    public void updateModenemy1(boolean modEnemy1){
        this.modEnemy1 = modEnemy1;
    }
    //Ignored if modstat2 = 0
    public void updateModenemy2(boolean modEnemy2){
        this.modEnemy2 = modEnemy2;
    }
    //Required
    public String getName(){
        return name;
    }
    //Required
    public String updateElement(){
        return element;
    }
    //Required
    public int getID(){
        return id;
    }
    //Required
    public int getType(){
        return type;
    }
    //Ignored if 0
    public int getDamage(){
        return damage;
    }
    //Ignored if 0
    public int getAbsorb(){
        return absorb;
    }
    //Ignored if 0
    public int getModstat1(){
        return modstat1;
    }
    //Ignored if 0
    public int getModstat2(){
        return modstat2;
    }
    //Ignored if modstat1 = 0
    public int getModamount1(){
        return modamount1;
    }
    //Ignored if modstat2 = 0
    public int getModamount2(){
        return modamount2;
    }
    //Required (0 indicates normal attacks, ect)
    public int getDuration(){
        return duration;
    }
    //Ignored if modstat1 and modstat2 = 0
    public int getDebuffDuration(){
        return debuffDuration;
    }
    //Required
    public boolean getForceFirst(){
        return forceFirst;
    }
    //Required
    public boolean getForceLast(){
        return forceLast;
    }
    //Ignored if modstat1 = 0
    public boolean getModenemy1(){
        return modEnemy1;
    }
    //Ignored if modstat2 = 0
    public boolean getModenemy2(){
        return modEnemy2;
    }
    public String getTrueName(int pokeID){
        String str = getName();
        for (int i = 0; i < this.pokeID.size(); i++){
            if (this.pokeID.get(i) == pokeID)
                str = newName.get(i);
        }
        return str;
    }
}
