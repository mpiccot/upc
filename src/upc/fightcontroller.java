/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

/**
 *
 * @author Mike
 */
public class fightcontroller {
    private int totalHP, currentHP, totalStamina, currentStamina, ID, level;
    private int eTotalHP, eCurrentHP, eTotalStamina, eCurrentStamina, eID, eLevel;
    private String name;
    private String eName;
    private pokehandler handler;
    //im pretty sure this passes by refrence, not sure if by constant
    //if you make any changes to pokehandler aften the initial setup, this could cause issues here
    public fightcontroller(pokehandler handler){
        this.handler = handler;
    }
    public void changePoke(int ID){
        name = handler.getPoke(ID).getName();
        totalHP = handler.getPoke(ID).getHP() * level;
        totalStamina = handler.getPoke(ID).getStamina() * level;
        this.ID = ID;
        currentHP = 40;
        currentStamina = 40;
    }
    public void eChangePoke(int eID){
        eName = handler.getPoke(ID).getName();
        eTotalHP = handler.getPoke(ID).getHP() * eLevel;
        eTotalStamina = handler.getPoke(ID).getStamina() * eLevel;
        this.ID = eID;
        eCurrentHP = 40;
        eCurrentStamina = 40;
    }
    public void setECurrentHP(int eCurrentHP){
        this.eCurrentHP = eCurrentHP;
    }
    public void setCurrentHP(int currentHP){
        this.currentHP = currentHP;
    }
    public String getName(){
        return name;
    }
    public String getEName(){
        return eName;
    }
    public int getTotalHP(){
        return totalHP;
    }
    public int getCurrentStamina(){
        return currentStamina;
    }
    public int getTotalStamina(){
        return totalStamina;
    }
    public int getID(){
        return ID;
    }
    public int getLevel(){
        return level;
    }
    public int getETotalHP(){
        return eTotalHP;
    }
    public int getECurrentStamina(){
        return eCurrentStamina;
    }
    public int getETotalStamina(){
        return eTotalStamina;
    }
    public int getEID(){
        return eID;
    }
    public int getELevel(){
        return eLevel;
    }
}
