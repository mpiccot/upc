package upc;

//import upc.InputHandler;

import java.awt.*;
import java.awt.event.KeyEvent; 
import java.awt.image.BufferedImage; 
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame; 
//import upc.gridhandler;

/** 
 * Main class for the game 
 */ 
public class UPC extends JFrame 
{        
        boolean isRunning = true; 
        int fps = 30; 
        int windowWidth = 600; 
        int windowHeight = 600;
        //This is your map height and width AKA the dimensions of the map picture
        int mapWidth = 6000;
        int mapHeight = 6000;
        //this is x y coord of your arena on your background
        //int arenax = 3990, arenay = 4380;
        //This is how far you move in a key press, making the game grid
        int movedistance = 30;
        int x = 390;
        int y = 5070;
        //This is your target, where you character will move to per key press
        int targetx = x, targety = y;
        //This is your direction, only 1 or -1
        int directionx = 1, directiony = 0;
        //Your last moved direction
        int lastmoved = -1;
        //Ticker for player sprite movement
        int playerTicker = 0;
        //are you in a fight?
        boolean fight = false;
        //immune to wormholes? this makes it so you can't get spam teleported
        boolean wormholeimmune = false;
        //What current bagindex you and your enemy are "on"
        int bagIndex = 0, enemyBagIndex = 0;
        
        BufferedImage backBuffer, background, splash;
        BufferedImage playerSprite;
        BufferedImage arenaSprite, enemyPokeSprite, friendlyPokeSprite, arrowSprite;
        //DEBUGGING RED SQUARE
        BufferedImage redsquare;
        Insets insets; 
        InputHandler input;
        gridhandler grid;
        wormholehandler worm;
        wormhole currentWorm;
        pokehandler pokehand;
        movehandler movehand;
        bag myBag, enemyBag;
        Font font = new Font("TimesRoman", Font.BOLD, 24);

        

        
        public static void main(String[] args) throws IOException 
        { 
                UPC game = new UPC(); 
                game.run(); 
                System.exit(0); 
        } 
        
        /** 
         * This method starts the game and runs it in a loop 
         */ 
        public void run() throws IOException 
        { 
                initialize(); 
                
/*                final SplashScreen splash = SplashScreen.getSplashScreen();
                if (splash == null) {
                    System.out.println("SplashScreen.getSplashScreen() returned null");
                    return;
                }
                Graphics2D g = splash.createGraphics();
                if (g == null) {
                    System.out.println("g is null");
                    return;
                }*/
                
                while(isRunning) 
                { 
                        long time = System.currentTimeMillis(); 
                        if (fight == false){
                            update(); 
                            draw();
                        }
                        else{
                            fightUpdate();
                            fightDraw();
                        }
                            
                        
                        //  delay for each frame  -   time it took for one frame 
                        time = (1000 / fps) - (System.currentTimeMillis() - time); 
                        
                        if (time > 0) 
                        { 
                                try 
                                { 
                                        Thread.sleep(time); 
                                } 
                                catch(Exception e){} 
                        } 
                } 
                
                setVisible(false); 
        } 
        
        /** 
         * This method will set up everything need for the game to run 
         */ 
        void initialize() throws IOException
        { 
                //Setup screen
                setTitle("UPQ"); 
                setSize(windowWidth, windowHeight); 
                setResizable(false); 
                setDefaultCloseOperation(EXIT_ON_CLOSE); 
                 
                //Didn't use insets, might go back and change this
                insets = getInsets(); 
                setSize(windowWidth, 
                                windowHeight);
                
                //Open up the screen and draw splash
                setVisible(true);
                if ((System.currentTimeMillis() % 2) == 0)
                    splash = ImageIO.read(new File("images/splash/splash.png"));
                else
                    splash = ImageIO.read(new File("images/splash/splash2.png"));
                //prints the splash screen for .25 seconds... for some reason this makes it stay i guess
                long timer = System.currentTimeMillis();
                long timer2 = timer + 250;
                while (timer < timer2){
                    drawSplash();
                    timer = System.currentTimeMillis();
                }
                //get the time splash was drawn at
                long time = System.currentTimeMillis();
                //load other images
                backBuffer = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_RGB);
                background = ImageIO.read(new File("images/debug/Background.jpg"));
                
                //Locs for psprite: 29 accross, 31 vertical
                //all y start at px 8, down 30, using - xval(width) 8(31) for y
                //Left - 29 (no space) - 274(29) - 310(29, mov)
                //Right - 29 (no space) - 204(29) - 238(29, move)
                //Up - 27 (1 on each side) - 441(29) 473(29,mov1) 503(29,mov2)
                //Down - 27 (1 on each side) - 343(29) 133(29,mov1) 167(29,mov2)
                playerSprite = ImageIO.read(new File("images/player/pokemon.png"));
                redsquare = ImageIO.read(new File("images/redsquare.jpg"));
                arenaSprite = ImageIO.read(new File("images/battle/testimage.png"));
                arrowSprite = ImageIO.read(new File("images/menu/arrow.png"));
                
                //setup input handler
                input = new InputHandler(this);
                //setup a new grid
                grid = new gridhandler(movedistance, mapWidth, mapHeight, "ini/movement.ini");
                //make the grid (this takes a long time, think of a better way? can i do it in parallel?)
                grid.makeGrid();
                worm = new wormholehandler(grid);
                worm.wormholeReader(windowWidth, windowHeight, "ini/wormhole.ini");
                currentWorm = null;
                //setup the reader to read from the ini and make poke list, the index is the ID
                pokehand = new pokehandler();
                pokehand.pokeReader("ini/poke.ini");
                movehand = new movehandler();
                movehand.moveReader("ini/moves.ini");
                //DEBUGGING output first two pokes
                System.out.println(pokehand.returnPoke(0));
                System.out.println(pokehand.returnPoke(1));
                //Make bags
                myBag = new bag();
                enemyBag = new bag();
                //DEBUGGING YOU GET A FREE LEVEL 10 FIRST POKE AND THIRD
                myBag.addPoke(pokehand.getPoke(0), 10);
                myBag.addPoke(pokehand.getPoke(2), 10);
                myBag.getItem(1).setHP(pokehand.getPoke(myBag.getItem(1).getID()).getHP()*5);
                //DEBUGGING ENEMY HAS LEVEL 10 SECOND POKE
                enemyBag.addPoke(pokehand.getPoke(1), 10);
                enemyBag.dropPoke(0);
                enemyBag.addPoke(pokehand.getPoke(1), 10);
                //set the computer to sleep for some time, this makes it so your splash screen always displays for 2.5 seconds at least
                //if it takes longer than 2.5 seconds to load, this goes negative, and the thread doesnt sleep
                System.out.println("Sleeping for: " + (time - System.currentTimeMillis() + 2500));
                try{
                    Thread.sleep(time - System.currentTimeMillis() + 2250);}
                    catch(Exception e){}
                
        } 
        
        //updates of walking around part of the game
        void update() throws IOException 
        { 
            
            //FIX, you can still move diagonally
            //once you get to your target and holding both keys...
            //get into loop, set a new x target, then a new y target
            //limit your y target if you have an x target already
                if ((targety == y) && (targetx == x)){
                    //check what way you are going
                    if (input.isKeyDown(KeyEvent.VK_RIGHT))
                    {
                        //no matter if its legal or not, set that as your last moved, and thus, changes your orintation
                        lastmoved = 1;
                        //if its legal, also set a target location so you can start moving that direction
                        if (grid.isLegal((x+movedistance+300),y+300) == true){
                            targetx = x + movedistance;
                            directionx = 1;
                        }
                    } 
                    if (input.isKeyDown(KeyEvent.VK_LEFT)) 
                    {
                        lastmoved = -1;
                        if (grid.isLegal((x-movedistance+300),y+300) == true){
                            targetx = x - movedistance;
                            directionx = -1;
                        }
                    }
                    if (targetx == x){
                    if (input.isKeyDown(KeyEvent.VK_DOWN)) 
                    {
                        lastmoved = 2;
                        if (grid.isLegal(x+300,(y+movedistance+300)) == true){
                            targety = y + movedistance;
                            directiony = 1;
                        }
                    } 
                    if (input.isKeyDown(KeyEvent.VK_UP)) 
                    {
                        lastmoved = -2;
                        if (grid.isLegal(x+300,(y-movedistance+300)) == true){
                            targety = y - movedistance;
                            directiony = -1;
                        }
                    }}
                    }
                //physically change x and y (moves the screen) if your target is diff from your current
                //also, add a last moved direction 1 = right -1 = left 2 = down -2 = up
                if (targetx != x){
                        x += (5 * directionx);
                        lastmoved = directionx;
                        wormholeimmune = false;
                    }
                else{
                    System.out.println("checking on x...");
                    currentWorm = worm.checkForWormhole(x, y);
                    if ((currentWorm != null) && (wormholeimmune == false)){
                        System.out.println("FOUND");
                        System.out.println(currentWorm.getInX()+","+currentWorm.getInY());
                        x = currentWorm.getOutX();
                        y = currentWorm.getOutY();
                        targetx = x;
                        targety = y;
                        wormholeimmune = true;
                    }
                    directionx = 0;
                }
                if (targety != y){
                        y += (5 * directiony);
                        lastmoved = directiony*2;
                        wormholeimmune = false;
                    }
                else{
                    currentWorm = worm.checkForWormhole(x, y);
                    if ((currentWorm != null) && (wormholeimmune == false)){
                        x = currentWorm.getOutX();
                        y = currentWorm.getOutY();
                        targetx = x;
                        targety = y;
                        wormholeimmune = true;
                    }
                    directiony = 0;
                }
            //used for animations on moving... but this ONLY works while you are moving
            if (playerTicker == 8)
                playerTicker = 0;
            playerTicker++;
            //if you aren't moving turn off the player ticker
            if ((directionx == 0) && (directiony == 0))
                playerTicker = 0;
            //DEBUGGING OUTPUT X Y OF THE BACKGROUND
            System.out.println(x+","+y);
            //DEBUGGING TO START A FIGHT HIT P
            if (input.isKeyDown(KeyEvent.VK_P)){
                fight = true;
                newFightPoke();
            }
        } 
        
        //drawing for walking around part
        void draw() 
        {
                Graphics g = getGraphics(); 
                //set up the back buffer (im actually not sure what this does, but i seem to need it)
                Graphics bbg = backBuffer.getGraphics(); 
                
                bbg.setColor(Color.WHITE); 
                bbg.fillRect(0, 0, windowWidth, windowHeight);
                
                //old buffer with insets
               // g.drawImage(backBuffer, insets.left, insets.top, this);
                //draw the background as an offset of your current x y coords of the player, this means when the player "moves" it actually just
                //draws the background in the opposite direction
                g.drawImage(background, -x, -y , this);
                //DEBUGGING, draw red boxes where you can't go, only draws in screen range (things EXPLODE if you don't, at least in NB)
                for (int zz = 0; zz < (mapWidth/movedistance); zz++){
                    for (int z = 0; z < (mapHeight/movedistance); z++){
                        if ((grid.getVal(zz,z) == false) && (zz-(x/movedistance) >= 0) && (zz-(x/movedistance) <= windowWidth) && 
                                (z-(y/movedistance) >= 0) && (z-(y/movedistance) <= windowHeight))
                            g.drawRect(((zz*movedistance)-x), ((z*movedistance)-y), 30, 30);    
                    }
                }               
                //END DEBUGGING
                //draw a rect around the player... for some reason
                g.drawRect(300, 300, 30, 30);
                //if going right, left, down, up respectivily, then move that direction, using playerticker to determinte the animation
                //the player is ALWAYS drawn at the center of the screen... also note you can only do one of these at a time
                if (directionx == 1){
                    if (playerTicker < 4)
                        g.drawImage(playerSprite.getSubimage(204, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else
                        g.drawImage(playerSprite.getSubimage(238, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                }
                else if (directionx == -1){
                    if (playerTicker < 4)
                        g.drawImage(playerSprite.getSubimage(274, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else
                        g.drawImage(playerSprite.getSubimage(310, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                }
                else if (directiony == 1){
                    if (playerTicker < 4)
                        g.drawImage(playerSprite.getSubimage(133, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else
                        g.drawImage(playerSprite.getSubimage(167, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                }
                else if (directiony == -1){
                    if (playerTicker < 4)
                        g.drawImage(playerSprite.getSubimage(473, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else
                        g.drawImage(playerSprite.getSubimage(503, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                }
                //if you are idle, then just face in the last direction you went in
                else{
                    if (lastmoved == 1)
                        g.drawImage(playerSprite.getSubimage(204, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else if (lastmoved == -1)
                        g.drawImage(playerSprite.getSubimage(274, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else if (lastmoved == 2)
                        g.drawImage(playerSprite.getSubimage(343, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else if (lastmoved == -2)
                        g.drawImage(playerSprite.getSubimage(441, 8, 29, 31), (windowWidth/2), (windowHeight/2), null);
                    else
                        System.out.println("ERROR IN PLAYER SPRITE");
                }
                
                                
        }
        //Vars used only in fighttime
        //x and y locs of the arrow on the menu
        int arrowx = 70, arrowy = 475;
        //where are you in the menu, can be main, change, item, text (for displaying text)
        String menu = "main";
        boolean zpressed = false, xpressed = false;
        
        //DEBUGGING TAKE THE IOEXCEPTION OFF THE UPDATE
        void fightUpdate() throws IOException
        {
            if (input.isKeyDown(KeyEvent.VK_RIGHT)){
                arrowx = 270;
            }
            else if (input.isKeyDown(KeyEvent.VK_LEFT)){
                arrowx = 70;
            }
            if (input.isKeyDown(KeyEvent.VK_UP)){
                arrowy = 475;
            }
            else if (input.isKeyDown(KeyEvent.VK_DOWN)){
                arrowy = 525;
            }
            if (input.isKeyDownOnce(KeyEvent.VK_Z)){
                if (arrowx == 70){
                    if (arrowy == 475){
                        if (menu.equals("main"))
                            //Attack
                            menu = "attack";
                        else if (menu.equals("attack")){
                            //do stuff for attack 1
                            selection = "move1";
                            menu = "attack";
                            doRound();
                        }
                    }
                    else{
                        if (menu.equals("main"))
                            //Run
                            menu = "text";
                        else if (menu.equals("attack")){
                            //do stuff for attack 2
                            selection = "move2";
                            menu = "attack";
                            doRound();
                        }
                    }
                }
                else{
                    if (arrowy == 475){
                        if (menu.equals("main")){
                            //Change
                            menu = "main";
                            //DEBUGGING CHANGE BAG INDEX
                            if (bagIndex == 1)
                                bagIndex = 0;
                            else
                                bagIndex = 1;
                            newFightPoke();
                            
                        }
                        else if (menu.equals("attack")){
                            //do stuff for attack 3
                            selection = "move3";
                            menu = "attack";
                            doRound();
                        }
                    }
                    else{
                        if (menu.equals("main"))
                            //Item
                            menu = "main";
                        else if (menu.equals("attack")){
                            //do stuff for attack 4
                            selection = "move4";
                            menu = "attack";
                            doRound();
                        }
                    }
                }       
            }
            if (input.isKeyDownOnce(KeyEvent.VK_X)){
                menu = "main";
            }
            //DEBUGGING TO EXIT A FIGHT HIT O
            if (input.isKeyDownOnce(KeyEvent.VK_O)){
                arrowx = 70;
                arrowy = 475;
                menu = "main";
                fight = false;
            }
            
        }
        //Does stuff for bringing out a new poke, or enemy switch, or starting fight or whatever
        String myMove1, myMove2, myMove3, myMove4;
        void newFightPoke() throws IOException{
            String str;
            str = pokehand.getPoke(myBag.getItem(bagIndex).getID()).getName();
            friendlyPokeSprite = ImageIO.read(new File("images/characters/"+str+"/"+str+".png"));
            str = pokehand.getPoke(enemyBag.getItem(enemyBagIndex).getID()).getName();
            enemyPokeSprite = ImageIO.read(new File("images/characters/"+str+"/"+str+".png"));
            myMove1 = movehand.getMove(myBag.getItem(bagIndex).getMove1()).getTrueName(myBag.getItem(bagIndex).getID());
            myMove2 = movehand.getMove(myBag.getItem(bagIndex).getMove2()).getTrueName(myBag.getItem(bagIndex).getID());
            myMove3 = movehand.getMove(myBag.getItem(bagIndex).getMove3()).getTrueName(myBag.getItem(bagIndex).getID());
            myMove4 = movehand.getMove(myBag.getItem(bagIndex).getMove4()).getTrueName(myBag.getItem(bagIndex).getID());
            myHPbar = (double) (myBag.getItem(bagIndex).getHP()) / (double) (pokehand.getPoke(myBag.getItem(bagIndex).getID()).getHP() * myBag.getItem(bagIndex).getLevel());
            enemyHPbar = (double) (enemyBag.getItem(enemyBagIndex).getHP()) / (double) (pokehand.getPoke(enemyBag.getItem(enemyBagIndex).getID()).getHP() * enemyBag.getItem(enemyBagIndex).getLevel());
        }
        String selection = "";
        move myMove;
        int myHeal, myHealDur, myDot, myDotDur;
        int eHeal, eHealDur, eDot, eDotDur;
        //DEBUGGING
        String debugString = "";
        void doRound(){
            //FIRST CHECK FOR SWAPS
            //IF SWAPS EXIST, DO THEM
            
            //SECOND CHECK FOR ITEM USES
            //IF USING AN ITEM, USE THEM
            
            //THIRD APPLY DOTS
            //do heals/dots/debuff after using a move or item or swap
            if (myDotDur > 0){
                System.out.println("subbing dot hp");
                myBag.getItem(bagIndex).subHP(myDot);
                myDotDur--;
                myHPbar = (double) (myBag.getItem(bagIndex).getHP()) / (double) (pokehand.getPoke(myBag.getItem(bagIndex).getID()).getHP() * myBag.getItem(bagIndex).getLevel());
            }
            if (eDotDur > 0){
                System.out.println("subbing e dot hp");
                enemyBag.getItem(enemyBagIndex).subHP(eDot);
                eDotDur--;
                enemyHPbar = (double) (enemyBag.getItem(enemyBagIndex).getHP()) / (double) (pokehand.getPoke(enemyBag.getItem(enemyBagIndex).getID()).getHP() * enemyBag.getItem(enemyBagIndex).getLevel());
            }
            //FOURTH APPLY HOTS
            if (myHealDur > 0){
                System.out.println("adding hp");
                myBag.getItem(bagIndex).subHP(myHeal);
                myHealDur--;
                myHPbar = (double) (myBag.getItem(bagIndex).getHP()) / (double) (pokehand.getPoke(myBag.getItem(bagIndex).getID()).getHP() * myBag.getItem(bagIndex).getLevel());
            }
            if (eHealDur > 0){
                System.out.println("adding e hp");
                enemyBag.getItem(enemyBagIndex).subHP(eHeal);
                eHealDur--;
                enemyHPbar = (double) (enemyBag.getItem(enemyBagIndex).getHP()) / (double) (pokehand.getPoke(enemyBag.getItem(enemyBagIndex).getID()).getHP() * enemyBag.getItem(enemyBagIndex).getLevel());
            }
            //FIFTH APPLY BUFFS/DEBUFFS
            //SIXTH REMOVE DEBUFFS AND DOTS AND HOTS
            if ((myDot > 0) && (myDotDur == 0)){
                myDot = 0;
                //FINISH ME
                //FINISH ME
                //FINISH ME
            }
            if (selection.substring(0, 4).equals("move")){
                myMove = new move();
                if (selection.equals("move1")){
                    myMove = movehand.getMove(myBag.getItem(bagIndex).getMove1());
                }
                else if (selection.equals("move2")){
                    myMove = movehand.getMove(myBag.getItem(bagIndex).getMove2());
                }
                else if (selection.equals("move3")){
                    myMove = movehand.getMove(myBag.getItem(bagIndex).getMove3());
                }
                else if (selection.equals("move4")){
                    myMove = movehand.getMove(myBag.getItem(bagIndex).getMove4());
                }
                if (myMove.getDamage() > 0){
                    //if there is a duration
                    if (myMove.getDuration() > 0){
                        if (eDotDur == 0){
                            eDot = myMove.getDamage();
                            eDotDur = myMove.getDuration();
                            debugString = "You used " + myMove.getTrueName(myBag.getItem(bagIndex).getID()) + " and applied " + myMove.getDamage() + " damage for " + myMove.getDuration() + " turn(s) DOT";
                        }
                        else
                            debugString = "Already dotted";
                    }
                    //if no duration
                    else{
                        enemyBag.getItem(enemyBagIndex).subHP(myMove.getDamage());
                        enemyHPbar = (double) (enemyBag.getItem(enemyBagIndex).getHP()) / (double) (pokehand.getPoke(enemyBag.getItem(enemyBagIndex).getID()).getHP() * enemyBag.getItem(enemyBagIndex).getLevel());
                        debugString = "You used " + myMove.getTrueName(myBag.getItem(bagIndex).getID()) + " and did " + myMove.getDamage() + " damage, enemy has " + enemyBag.getItem(enemyBagIndex).getHP();
                    }
                }
                //damage < 0, hurt yourself
                else if (myMove.getDamage() < 0){
                    //NOTE: you can't dot yourself (or else you could overwrite big dots with small ones)
                    myBag.getItem(bagIndex).subHP(myMove.getDamage());
                    myHPbar = (double) (myBag.getItem(bagIndex).getHP()) / (double) (pokehand.getPoke(myBag.getItem(bagIndex).getID()).getHP() * myBag.getItem(bagIndex).getLevel());
                    debugString = "You used " + myMove.getTrueName(myBag.getItem(bagIndex).getID()) + " and did " + myMove.getDamage() + " damage to self, you have " + myBag.getItem(bagIndex).getHP();
                }
                if (myMove.getAbsorb() > 0){
                    //if absorb is greater than max hp
                    if (myMove.getAbsorb() > ((pokehand.getPoke(myBag.getItem(bagIndex).getID()).getHP() * myBag.getItem(bagIndex).getLevel()) - myBag.getItem(bagIndex).getHP())){
                        myBag.getItem(bagIndex).setHP(pokehand.getPoke(myBag.getItem(bagIndex).getID()).getHP() * myBag.getItem(bagIndex).getLevel());
                    }
                    else{
                        myBag.getItem(bagIndex).subHP(-myMove.getAbsorb());
                    }
                    myHPbar = (double) (myBag.getItem(bagIndex).getHP()) / (double) (pokehand.getPoke(myBag.getItem(bagIndex).getID()).getHP() * myBag.getItem(bagIndex).getLevel());
                }
            }
            selection = "";
        }
        double enemyHPbar = 1, myHPbar = 1;
        void fightDraw()
        {
            Graphics g = getGraphics(); 
                Graphics bbg = backBuffer.getGraphics(); 
                
                bbg.setColor(Color.WHITE);
                bbg.fillRect(0, 0, windowWidth, windowHeight);
                g.setFont(font);
                g.drawImage(arenaSprite, 0, 0 , null);
                g.drawImage(friendlyPokeSprite, 50, 170, null);
                g.drawImage(enemyPokeSprite, 350, 170, null);
                g.setColor(Color.GREEN);
   /*             if (friendlyhp% < 75)
                    g.setColor(Color.YELLOW);
                else if (friendly hp < 25)
                    g.setColor(Color.RED);*/
                g.fillRect(50, 40, (int) (myHPbar * 150.0), 20);
                g.setColor(Color.GREEN);
       /*         if (enemyhp% < 75)
                    g.setColor(Color.YELLOW);
                else if (enemyhp% < 25)
                    g.setColor(Color.RED);*/
                g.fillRect(400, 40, (int) (enemyHPbar * 150.0), 20);
                g.setColor(Color.YELLOW);
                g.fillRect(50, 70, 150, 10);
                g.fillRect(400, 70, 150, 10);
                g.setColor(Color.BLACK);
                
                if (menu.equals("main")){
                g.drawString("Attack", 100, 500);
                g.drawString("Run", 100, 550);
                g.drawString("Change", 300, 500);
                g.drawString("Item", 300, 550);
                g.drawImage(arrowSprite, arrowx, arrowy, null);
                }
                else if (menu.equals("attack")){
                g.drawString(myMove1, 100, 500);
                g.drawString(myMove2, 100, 550);
                g.drawString(myMove3, 300, 500);
                g.drawString(myMove4, 300, 550);
                g.drawImage(arrowSprite, arrowx, arrowy, null);
                }
                else if (menu.equals("text")){
                    g.drawString("You run from a poke, and you are now", 100, 500);
                    g.drawString("less of a man because of it.", 100, 550);
                    try{Thread.sleep(3000);}
                    catch(Exception e){}
                    arrowx = 70;
                    arrowy = 475;
                    menu = "main";
                    fight = false;
                }
                //DEBUGGING OUTPUT TEXT
                g.setColor(Color.GREEN);
                g.drawString(debugString.substring(0, debugString.length()/2), 10, 300);
                g.drawString(debugString.substring(debugString.length()/2), 10, 350);
                //DRAW FIGHT AREA STUFF
                //DRAW ARROW USING IF STATEMENTS
                g.setColor(Color.WHITE);
                
        }
        void drawSplash()
        {
            //Graphics bbg = backBuffer.getGraphics(); 
            //bbg.setColor(Color.WHITE);
            //bbg.fillRect(0, 0, windowWidth, windowHeight);
            Graphics g = getGraphics();
            g.drawImage(splash, 0, 0, this);
            
        }
        void updateMenu()
        {
            
        }
        void drawMenu()
        {
            Graphics g = getGraphics();
            g.setFont(new Font("Courier", Font.BOLD, 30));
            g.setColor(Color.red);
            g.drawString("sup", 100, 100);
        }
        BufferedImage flip(BufferedImage img){
            return img;
        }
} 
