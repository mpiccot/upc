/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package upc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Mike
 */
public class movehandler {
    private ArrayList<move> move = new ArrayList<>();
    public movehandler(){}
    public void moveReader(String data) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(data));
        String line = "", tempstring = "";
        int linenum = 0;
        int[] params = new int[10];
        boolean[] params2 = new boolean[4];
        move tempmove = new move();
        ArrayList<Integer> temparray1 = new ArrayList<>();
        ArrayList<String> temparray2 = new ArrayList<>();
        while ((line = br.readLine()) != null){
            if (line.length() > 0){
            if (line.charAt(0) != ';'){
                if (linenum == 0){
                    tempmove = new move();
                    tempmove.updateName(line.substring(5));
                    linenum++;
                }
                else if (linenum == 1){
                    tempmove.updateElement(line.substring(8));
                    linenum++;
                }
                else if (linenum < 12){
                    params[linenum-2] = Integer.parseInt(line.substring(line.indexOf('=')+1));
                    linenum++;
                }
                else if (linenum < 16){
                    params2[linenum-12] = Boolean.parseBoolean(line.substring(line.indexOf('=')+1));
                    linenum++;
                }
                else if (linenum == 16){
                    temparray2.clear();
                    tempmove.updateID(params[0]);
                    tempmove.updateType(params[1]);
                    tempmove.updateDamage(params[2]);
                    tempmove.updateAbsorb(params[3]);
                    tempmove.updateModstat1(params[4]);
                    tempmove.updateModstat2(params[5]);
                    tempmove.updateModamount1(params[6]);
                    tempmove.updateModamount2(params[7]);
                    tempmove.updateDebuffDuration(params[8]);
                    tempmove.updateDuration(params[9]);
                    tempmove.updateModenemy1(params2[0]);
                    tempmove.updateModenemy2(params2[1]);
                    tempmove.updateForceFirst(params2[2]);
                    tempmove.updateForceLast(params2[3]);
                    line = line.substring(8);
                    //FIX THIS IT DOESNT DO ANYTHING RIGHT NOW
                    while (line.isEmpty() == false){
                        temparray2.add((line.substring(0, line.indexOf(','))));
                        line = line.substring(line.indexOf(',')+1);
                    }
                    tempmove.updateNameList(temparray2);
                    linenum++;
                }
                else if (linenum == 17){
                    temparray1.clear();
                    line = line.substring(7);
                    //FIX THIS IS DOESNT DO ANYTHING RIGHT NOW
                    while (line.isEmpty() == false){
                        temparray1.add(Integer.parseInt(line.substring(0, line.indexOf(','))));
                        line = line.substring(line.indexOf(',')+1);
                    }
                    tempmove.updatePokeIDList(temparray1);
                    linenum++;
                }
            }
            }
            if (linenum == 18){
                    addMove(tempmove);
                    //clear params
                    linenum = 0;
                    tempstring = "";
                    temparray1.clear();
                    temparray2.clear();
                }
        }
        br.close();
    }
    public void addMove(move ability){
        move.add(ability);
    }
    public move getMove(int ID){
        return move.get(ID);
    }

}
